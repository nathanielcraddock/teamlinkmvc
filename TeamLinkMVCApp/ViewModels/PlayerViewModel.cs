﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.ViewModels
{
    public class PlayerViewModel
    {
        public PlayerViewModel()
        {
            LastSearched = DateTime.Now;
        }


        public int PlayerID { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Gamertag { get; set; }
        [Required]
        public string PlaystationID { get; set; }
        [Required]
        public string SteamID { get; set; }
        [DataType(DataType.DateTime)]
        [Required]
        [Display(Name = "Date & Time")]
        public DateTime? LastSearched { get; set; }
        [Range(1, 9)]
        [Display(Name = "Region")]
        public int? Region { get; set; }
        [Range(1, 4)]
        [Display(Name = "GamerStyle")]
        public int? GamerStyle { get; set; }

    }
}