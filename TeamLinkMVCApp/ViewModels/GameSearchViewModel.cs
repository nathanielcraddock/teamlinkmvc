﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.ViewModels
{
    public class GameSearchViewModel
    {
        public GameSearchViewModel()
        {
            SearchTime = DateTime.Now;
        }

        public int SearchID { get; set; }
        [Required]
        public int PlayerID { get; set; }
        [Required]
        public int SystemID { get; set; }
        [Required]
        public int GameID { get; set; }
        [Required]
        public int GamerStyleID { get; set; }
        [DataType(DataType.DateTime)]
        [Required]
        [Display(Name = "Date & Time")]
        public DateTime SearchTime { get; set; }
        [Required]
        public int RegionID { get; set; }
        


    }
}
