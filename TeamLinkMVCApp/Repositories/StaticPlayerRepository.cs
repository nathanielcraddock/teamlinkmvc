﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.Repositories
{
    public class StaticPlayerRepository : IPlayerRepository
    {
        private static readonly ConcurrentDictionary<int, Player> Players = new ConcurrentDictionary<int, Player>();
        public Player GetById(int id)
        {
            return !Players.ContainsKey(id) ? null : Players[id];
        }
        public void Upsert(Player IncomingPlayer)
        {
            Players[IncomingPlayer.PlayerID] = IncomingPlayer;
        }
        public IEnumerable<Player> All { get { return Players.Values; } }

        public void UpdatePlayer(Player newplayer, Player existingPlayer)
        {
            Players.TryUpdate(newplayer.PlayerID, newplayer, existingPlayer);
        }
        public void DeletePlayer(int id)
        {
            Player dumbPlayer = new Player();
            Players.TryRemove(id, out dumbPlayer);
        }
    }
}