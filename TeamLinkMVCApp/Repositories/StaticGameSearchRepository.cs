﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.Repositories
{
    public class StaticGameSearchRepository : IGameSearchRepository
    {
        private static readonly ConcurrentDictionary<int, SearchingPlayer> SearchingPlayers = new ConcurrentDictionary<int, SearchingPlayer>();

        public SearchingPlayer GetByID(int ID)
        {
            return !SearchingPlayers.ContainsKey(ID) ? null : SearchingPlayers[ID];
        }
        public void Upsert(SearchingPlayer IncomingSearchingPlayer)
        {
            SearchingPlayers[IncomingSearchingPlayer.SearchID] = IncomingSearchingPlayer;
        }
        public IEnumerable<SearchingPlayer> All { get { return SearchingPlayers.Values; } }
    }
}