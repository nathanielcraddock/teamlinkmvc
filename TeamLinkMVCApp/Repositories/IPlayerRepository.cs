﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.Repositories
{
    public interface IPlayerRepository
    {
        Player GetById(int id);
        void Upsert(Player player);
        IEnumerable<Player> All { get; }
        void UpdatePlayer(Player player, Player existingPlayer);
        void DeletePlayer(int id);

    }
}