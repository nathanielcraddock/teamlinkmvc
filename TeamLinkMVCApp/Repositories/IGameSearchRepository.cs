﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;
using TeamLinkMVCApp.ViewModels;

namespace TeamLinkMVCApp.Repositories
{
    public interface IGameSearchRepository
    {
        SearchingPlayer GetByID(int ID);
        void Upsert(SearchingPlayer SearchingPlayers);
        IEnumerable<SearchingPlayer> All { get; }

    }
}