﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamLinkMVCApp.ViewModels;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;
namespace TeamLinkMVCApp.Controllers
{
    public class PlayerController : Controller
    {
        private IPlayerRepository playerRepository = new StaticPlayerRepository();
        //
        // GET: /Player/

        public ActionResult Index()
        {
            IEnumerable<Player> players = playerRepository.All;
            IList<PlayerViewModel> playerviewmodels = new List<PlayerViewModel>();
            foreach (Player p in players)
            {
                playerviewmodels.Add(ViewModelFromPlayer(p));
            }

            return View(playerviewmodels);
        }
        public ActionResult Create()
        {
            return View(new PlayerViewModel());
        }
        [HttpPost]
        public ActionResult Create(PlayerViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                //viewModel.Id = Guid.NewGuid();
                Player player = new Player();
                UpdatePlayer(player, viewModel);
                playerRepository.Upsert(player);
                using (PlayerDBEntities db = new PlayerDBEntities())
                {
                    db.Players.Add(player);
                    db.SaveChanges();
                    ModelState.Clear();
                    player = null;

                }
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }
        private void UpdatePlayer(Player player, PlayerViewModel viewModel)
        {
            player.PlayerID = viewModel.PlayerID;
            player.Email = viewModel.Email;
            player.Gamertag = viewModel.Gamertag;
            player.LastSearched = viewModel.LastSearched;
            player.PlaystationID = viewModel.PlaystationID;
            player.RegionID = viewModel.Region;
            player.SteamID = viewModel.SteamID;
            player.StyleID = viewModel.GamerStyle;
            
            
        }
        [HttpGet]
        public ActionResult Details(int ID)
        {
            Player p = playerRepository.GetById(ID);
            PlayerViewModel pvm = ViewModelFromPlayer(p);
            return View(pvm);

        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            Player p = playerRepository.GetById(ID);
            PlayerViewModel pvm = ViewModelFromPlayer(p);
            return View(pvm);
        }
        [HttpPost]
        //public ActionResult Edit(PlayerViewModel IncomingPlayer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        PlayerViewModel existingPlayer = playerRepository.GetById(IncomingPlayer.Id);
        //        playerRepository.UpdatePlayer(IncomingPlayer, existingPlayer);
        //        return RedirectToAction("Index");
        //    }
        //    return View(IncomingPlayer);
        //}
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            return View(playerRepository.GetById(ID));
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int ID)
        {
            playerRepository.DeletePlayer(ID);
            return RedirectToAction("Index");
        }
        private PlayerViewModel ViewModelFromPlayer(Player player)
        {
            PlayerViewModel viewModel = new PlayerViewModel
            {
                PlayerID = player.PlayerID,
                Email = player.Email,
                SteamID = player.SteamID,
                Gamertag = player.Gamertag,
                LastSearched = player.LastSearched,
                PlaystationID = player.PlaystationID,
                Region = player.RegionID,
                GamerStyle = player.StyleID
            };
            return viewModel;
        }
    }
}
