﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamLinkMVCApp.ViewModels;
using TeamLinkMVCApp.Repositories;
using TeamLinkMVCApp.Models;

namespace TeamLinkMVCApp.Controllers
{
    public class SearchingPlayerController : Controller
    {
        private IGameSearchRepository gamesearchRepository = new StaticGameSearchRepository();
        //
        // GET: /SearchingPlayer/



        public ActionResult SearchResults()
        {
            IEnumerable<SearchingPlayer> players = gamesearchRepository.All;
            IList<SearchingPlayer> searchviewmodels = new List<SearchingPlayer>();
            foreach (SearchingPlayer p in players)
            {
                searchviewmodels.Add(p);
            }

            return View(searchviewmodels);
        }
        //This is the create for the searching player
        public ActionResult Search()
        {
            return View(new GameSearchViewModel());
        }
        [HttpPost]
        public ActionResult Search(GameSearchViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                //viewModel.Id = Guid.NewGuid();
                SearchingPlayer searchingplayer = new SearchingPlayer();
                UpdatePlayer(searchingplayer, viewModel);
                gamesearchRepository.Upsert(searchingplayer);
                //using (PlayerDBEntities db = new PlayerDBEntities())
                //{
                    //db.SearchingPlayers.Add(searchingplayer);
                    //db.SaveChanges();
                    //ModelState.Clear();
                    //searchingplayer = null;

                //}
                return RedirectToAction("SearchResults");
            }
            return View(viewModel);
        }
        private void UpdatePlayer(SearchingPlayer player, GameSearchViewModel viewModel)
        {
            player.PlayerID = viewModel.PlayerID;
            player.RegionID = viewModel.RegionID;
            player.SearchID = viewModel.SearchID;
            player.SearchTime = viewModel.SearchTime;
            player.SystemID = viewModel.SystemID;
            player.GameID = viewModel.GameID;
            player.GamerStyleID = viewModel.GamerStyleID;
        }
        private GameSearchViewModel ViewModelFromPlayer(SearchingPlayer player)
        {
            GameSearchViewModel viewModel = new GameSearchViewModel
            {
                PlayerID = player.PlayerID,
                RegionID = player.RegionID,
                SearchID = player.SearchID,
                SearchTime = player.SearchTime,
                SystemID = player.SystemID,
                GameID = player.GameID,
                GamerStyleID = player.GamerStyleID,
            };
            return viewModel;
        }

    }
}
